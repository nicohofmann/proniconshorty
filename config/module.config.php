<?php

return array(
	'controllers' => array(
		'invokables' => array(
			'ProniconShorty\Controller\Trim' => 'ProniconShorty\Controller\TrimController',
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			__DIR__ . '/../view',
		),
	),
	'router' => array(
		'routes' => array(
			'pronicon-shorty-trim' => array(
				'type' => '\Zend\Mvc\Router\Http\Literal',
				'options' => array(
					'route' => '/trim',
					'defaults' => array(
						'controller' => 'ProniconShorty\Controller\Trim',
						'action' => 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					'list' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => '/list',
							'defaults' => array(
								'action' => 'list',
							),
						),
					),
					'delete' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => '/delete/[:id]',
							'defaults' => array(
								'action' => 'delete',
							),
							'constraints' => array(
								'id' => '[0-9]+',
							),
						),
					),
				),
			),
		),
	),
);